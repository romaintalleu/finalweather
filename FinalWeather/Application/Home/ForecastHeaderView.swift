//
//  ForecastHeaderView.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

class ForecastHeaderView: UIView {
    @IBOutlet private weak var cityLabel: UILabel!
    @IBOutlet private weak var countryLabel: UILabel!
    
    @IBOutlet private weak var leftLabel: UILabel!
    @IBOutlet private weak var leftCenterLabel: UILabel!
    @IBOutlet private weak var rightCenterLabel: UILabel!
    @IBOutlet private weak var rightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(header: ForecastHeaderViewModel) {
        self.cityLabel.text = header.cityName
        self.countryLabel.text = header.countryName
        
        self.leftLabel.text = header.highTemp
        self.leftCenterLabel.text = header.windSpeed
        self.rightCenterLabel.text = header.humidity
        self.rightLabel.text = ""
    }
}
