//
//  ForecastHourTableViewCell.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

class ForecastHourTableViewCell: UITableViewCell {
    @IBOutlet private weak var weatherIconImageView: UIImageView!
    @IBOutlet private weak var dayLabel: UILabel!
    @IBOutlet private weak var temperatureImageView: UIImageView!
    @IBOutlet private weak var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = ColorName.brown1.color
        self.dayLabel.textColor = ColorName.white1.color
        self.temperatureLabel.textColor = ColorName.white1.color
        
        self.selectionStyle = .none
    }
    
    func setup(forecastHour: ForecastHourViewModel) {
        self.dayLabel.text = forecastHour.hour
        self.weatherIconImageView.image = forecastHour.icon?.image()
        self.weatherIconImageView.tintColor = ColorName.white1.color
        
        self.temperatureLabel.text = forecastHour.temp
        self.temperatureImageView.image = UIImage(named: "temperature")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.temperatureImageView.tintColor = ColorName.white1.color
    }
}


