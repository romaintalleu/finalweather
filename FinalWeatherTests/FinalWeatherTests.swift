//
//  FinalWeatherTests.swift
//  FinalWeatherTests
//
//  Created by Romain Talleu on 03/01/2018.
//  Copyright © 2018 Romain Talleu. All rights reserved.
//

import XCTest
import RxSwift

private let kTimeout: TimeInterval = 15.0

class FinalWeatherTests: XCTestCase {
    
    private let disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchForecastSuccessFull() {
        let expectation = self.expectation(description: "WebService fetchRubrics fulfill")
        
        WWebServiceClient().fetchForecast()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { forecast in
                XCTAssertNotNil(forecast.list)
                XCTAssertFalse(forecast.list?.isEmpty ?? true)
            }, onError: { error in
                print(error)
            }, onCompleted: nil, onDisposed: {
                expectation.fulfill()
            })
            .addDisposableTo(self.disposeBag)
        
        self.waitForExpectations(timeout: kTimeout) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}
