//
//  City.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class City: Mappable {
    var identifier: Int?
    var name: String?
    var country: String?
    
    required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.identifier <- map["id"]
        self.name <- map["name"]
        self.country <- map["country"]
    }
}
