//
//  ICParameter.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import Alamofire

enum Parameter {
    case forecast
    
    var value: Parameters {
        switch self {
        case .forecast: return ["q": "Paris,fr",
                           "appid": "6e9a7f35a5f33af80b18ed2a526b1099"]
        }
    }
}
