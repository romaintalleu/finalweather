//
//  DateFormatter.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation

extension DateFormatter {
    static private let daySimpleDateFormat = "EEEE"
    static private let hourSimpleDateFormat = "H:mm"
    static let dayimpleDateFormatter = DateFormatter(format: daySimpleDateFormat)
    static let hourSimpleDateFormatter = DateFormatter(format: hourSimpleDateFormat)
    
    private convenience init(format: String) {
        self.init()
        dateFormat = format
    }
}
