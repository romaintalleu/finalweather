# FinalWeather

## ✅ CheckList démarrage de projet

### Actions manuelles à effectuer

* Ajuster la liste des pods dans le `Podfile` et relancer un `pod install` au besoin

### Services tiers à configurer

* Ajouter branche `develop` pour GitFlow et verouiller la branche `master` sur Bitbucket

### Architecture logicielle

* Utilisation d'Alamofire pour les requêtes réseau
