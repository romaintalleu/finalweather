//
//  ForecastDayViewModel.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation

struct ForecastDayViewModel {
    let icon: WeatherIcon?
    let dayString: String?
    let lowTemp: String?
    let highTemp: String?
    let hours: [ForecastHourViewModel]
    
    init(items: [ForecastItem]) {
        self.hours = items.map({ item -> ForecastHourViewModel in
            return ForecastHourViewModel(forecastItem: item)
        })
        self.icon = self.hours.first?.icon
        if let timestamp = items.first?.dt {
            let date = Date(timeIntervalSince1970: Double(timestamp))
            let dateFormatter = DateFormatter.dayimpleDateFormatter
            self.dayString = dateFormatter.string(from: date).uppercased()
        } else {
            self.dayString = "default".uppercased()
        }
        
        self.lowTemp = ""
        self.highTemp = ""
    }
}
