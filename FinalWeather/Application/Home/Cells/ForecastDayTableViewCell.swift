//
//  ForecastDayTableViewCell.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

protocol ForecastDayTableViewCellDelegate: class {
    func headerClicked(section: Int)
}

class ForecastDayTableViewCell: UITableViewHeaderFooterView {
    @IBOutlet private weak var weatherIconImageView: UIImageView!
    @IBOutlet private weak var dayLabel: UILabel!
    
    private var section: Int?
    var delegate: ForecastDayTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = ColorName.white1.color
        self.dayLabel.textColor = ColorName.brown1.color
    }
    
    func setup(forecastDay: ForecastDayViewModel, for section: Int) {
        self.section = section
        self.dayLabel.text = forecastDay.dayString
        self.weatherIconImageView.image = forecastDay.icon?.image()
        self.weatherIconImageView.tintColor = ColorName.brown1.color
    }
    
    @IBAction func headerClicked(_ sender: UIButton) {
        if let section = self.section {
            self.delegate?.headerClicked(section: section)
        }
    }
}
