//
//  ForecastHourViewModel.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation

struct ForecastHourViewModel {
    var hour: String?
    var icon: WeatherIcon?
    var temp: String?
    
    init(forecastItem: ForecastItem) {
        let date = Date(timeIntervalSince1970: Double(forecastItem.dt ?? 0))
        let dateFormatter = DateFormatter.hourSimpleDateFormatter
        self.hour = dateFormatter.string(from: date)
        
        
        self.icon = forecastItem.weather?.first?.icon
        self.temp = String(format: "%.0f °", (forecastItem.main?.temp ?? 0.0) - 273.15)
        
    }
}
