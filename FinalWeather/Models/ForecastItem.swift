//
//  ForecastItem.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class ForecastItem: Mappable {
    var dt: Int?
    var main: ForecastMain?
    var weather: [ForecastWeather]?
    var clouds: ForecastCloud?
    var wind: ForecastWind?
    var dtText: String?
    
    required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.dt <- map["dt"]
        self.main <- map["main"]
        self.weather <- map["weather"]
        self.clouds <- map["clouds"]
        self.wind <- map["wind"]
        self.dtText <- map["dt_txt"]
    }
}
