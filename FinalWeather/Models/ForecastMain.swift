//
//  ForecastMain.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class ForecastMain: Mappable {
    var temp: Double?
    var tempMin: Double?
    var tempMax: Double?
    var pressure: Double?
    var seaLevel: Double?
    var grndLevel: Double?
    var humidity: Int?
    var tempKF: Double?
    
    required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.temp <- map["temp"]
        self.tempMin <- map["temp_min"]
        self.tempMax <- map["temp_max"]
        self.pressure <- map["pressure"]
        self.seaLevel <- map["sea_level"]
        self.grndLevel <- map["grnd_level"]
        self.humidity <- map["humidity"]
        self.tempKF <- map["temp_kf"]
    }
}
