//
//  NetworkStack.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RxSwift

class NetworkStack {
    
    enum ErrorType: Error {
        case noInternet(error: Error)
        case serverUnreachable(error: Error)
        case badServerResponse(error: Error)
        case HTTP(statusCode: Int, data: Data?)
        case ParseError
        case otherError(error: Error)
        case unknow
    }
    
    private let requestManager: SessionManager
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.urlCache = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        self.requestManager = SessionManager(configuration: configuration)
    }
    
    func sendJSONRequest<T: Mappable>(url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil, responseType: T.Type) -> Observable<T> {
        let request = self.requestManager.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        return self.sendRequest(alamofireRequest: request, responseType: responseType)
    }
    
    func sendJSONRequest<T: Mappable>(url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil, responseType: T.Type) -> Observable<[T]> {
        let request = self.requestManager.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        return self.sendRequest(alamofireRequest: request, responseType: responseType)
    }
    
    // MARK: Private methods
    
    private func sendRequest<T: Mappable>(alamofireRequest: DataRequest, responseType: T.Type) -> Observable<T> {
        return Observable.create { observer in
            self.validateRequest(request: alamofireRequest).responseObject { (response: DataResponse<T>) in
                if let value = response.result.value, response.result.isSuccess {
                    observer.onNext(value)
                    observer.onCompleted()
                } else {
                    let stackError = self.networkStackError(error: response.error!, httpURLResponse: response.response, responseData: response.data)
                    observer.onError(stackError)
                    observer.onCompleted()
                }
            }
            
            return Disposables.create {
                alamofireRequest.cancel()
            }
        }
    }
    
    private func sendRequest<T: Mappable>(alamofireRequest: DataRequest, responseType: T.Type) -> Observable<[T]> {
        return Observable.create { observer in
            self.validateRequest(request: alamofireRequest).responseArray { (response: DataResponse<[T]>) in
                if let value = response.result.value, response.result.isSuccess {
                    observer.onNext(value)
                    observer.onCompleted()
                } else {
                    let stackError = self.networkStackError(error: response.error!, httpURLResponse: response.response, responseData: response.data)
                    observer.onError(stackError)
                    observer.onCompleted()
                }
            }
            
            return Disposables.create {
                alamofireRequest.cancel()
            }
        }
    }
    
    private func sendRequest<T: Mappable>(request: URLRequest, responseType: T.Type, success: (Mappable) -> Void, failed: ((ErrorType) -> Void)? = nil) {
        
    }
    
    private func networkStackError(error: Error, httpURLResponse: HTTPURLResponse?, responseData: Data? = nil) -> ErrorType {
        let finalError: ErrorType
        switch error {
        case URLError.notConnectedToInternet, URLError.cannotLoadFromNetwork, URLError.networkConnectionLost, URLError.callIsActive, URLError.internationalRoamingOff, URLError.dataNotAllowed, URLError.timedOut:
            finalError = ErrorType.noInternet(error: error)
        case URLError.cannotConnectToHost, URLError.cannotFindHost, URLError.dnsLookupFailed, URLError.redirectToNonExistentLocation:
            finalError = ErrorType.serverUnreachable(error: error)
        case URLError.badServerResponse, URLError.cannotParseResponse, URLError.cannotDecodeContentData, URLError.cannotDecodeRawData:
            finalError = ErrorType.badServerResponse(error: error)
        default:
            let statusCode = httpURLResponse?.statusCode ?? 0
            finalError = 400..<600 ~= statusCode ? ErrorType.HTTP(statusCode: statusCode, data: responseData) : ErrorType.otherError(error: error)
        }
        
        return finalError
    }
    
    private func validateRequest(request: DataRequest) -> DataRequest {
        return request.validate(statusCode: 200 ..< 300)
    }
}
