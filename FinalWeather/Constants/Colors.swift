//
//  Colors.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import UIKit

enum ColorName {
    case white1
    case brown1
    case yellow1
    case yellow2
    
    var rgbaValue: UInt32 {
        switch self {
        case .white1: return 0xCCCCCCff
        case .brown1: return 0x352b28ff
        case .yellow1: return 0xc4965cff
        case .yellow2: return 0xc08958ff
        }
    }
    
    var color: UIColor {
        return UIColor(named: self)
    }
}

extension UIColor {
    convenience init(named name: ColorName) {
        self.init(rgbaValue: name.rgbaValue)
    }
    
    convenience init(rgbaValue: UInt32) {
        let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
        let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
        let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
        let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
