//
//  Icon.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import UIKit

enum WeatherIcon: String {
    case d01 = "01d"
    case n01 = "01n"
    case d02 = "02d"
    case n02 = "02n"
    case d03 = "03d"
    case n03 = "03n"
    case d04 = "04d"
    case n04 = "04n"
    case d09 = "09d"
    case n09 = "00n"
    case d10 = "10d"
    case n10 = "10n"
    case d11 = "11d"
    case n11 = "11n"
    case d13 = "13d"
    case n13 = "13n"
    case d50 = "50d"
    case n50 = "50n"
    
    func image() -> UIImage {
        var image: UIImage!
        switch self {
        case .d01, .n01:
            image = UIImage(named: "01_black")!
        case .d02, .n02:
            image = UIImage(named: "02_black")!
        case .d03, .n03:
            image = UIImage(named: "03_black")!
        case .d04, .n04:
            image = UIImage(named: "04_black")!
        case .d09, .n09:
            image = UIImage(named: "09_black")!
        case .d10, .n10:
            image = UIImage(named: "10_black")!
        case .d11, .n11:
            image = UIImage(named: "11_black")!
        case .d13, .n13:
            image = UIImage(named: "13_black")!
        case .d50, .n50:
            image = UIImage(named: "50_black")!
        }
        image = image.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        return image
    }
}
