# FinalWeather

## Contexte

| Libellé | Description |
| --- | --- |
| Date de démarrage | 20/12/2017 |
| Plateformes | iOS9+ |
| Devices | Smartphone (Portrait) |
| Specs Fonctionnelles | Cf mail |

Note: Pas de créas ni de specs complexes

## Description

L'application FinalWeather est une application smartphone pour iOS, permettant de voir la météo du jour et les prévisions des jours à venir.

* [Site des WebServices](http://openweathermap.org/forecast5)

## Bonnes pratiques en vrac

* Préférer faire l'**Injection de Dépendances** (en particuler pour la class `WebServices`), c'est à dire passer l'objet `WebServices` de proche en proche aux Coordinators. Bannir les Singletons.
* Utiliser `let` dans la mesure du possible plutôt que `var`
* Réfléchir si un type ne doit pas une `struct` qu'une `class`
* Préférer utiliser des type `final`(faire des `final class` plutôt que `class`) dans la mesure du possible.
* N'oubliez pas d'utiliser un **Analyze** (Cmd-Shift-B) de temps en temps pour vérifier les reference cycles mais aussi pour vous rappeler les TODOs/FIXMEs à traiter.

## Composants et Patterns utilisés

* RxSwift
* [Reactivex.io](http://reactivex.io)
* [Tutoriels Rx généralistes](http://reactivex.io/tutorials.html)
* [Repo RxSwift avec docs, tutos et Playground](https://github.com/ReactiveX/RxSwift)
* Parsing des objets avec `ObjetMapper`
* Pattern MVVM
