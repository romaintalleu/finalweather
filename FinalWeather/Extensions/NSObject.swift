//
//  NSObject.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation

extension NSObject {
    static func className() -> String {
        return String(describing: self)
    }
}
