//
//  ForecastWind.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class ForecastWind: Mappable {
    var speed: Double?
    var deg: Int?
    
    required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.speed <- map["speed"]
        self.deg <- map["deg"]
    }
}
