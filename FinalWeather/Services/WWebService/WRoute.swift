//
//  ICRoute.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import Alamofire

public enum Route: URLConvertible {
    case forecast
    
    var path: String {
        switch self {
        case .forecast: return "/data/2.5/forecast"
        }
    }
    
    // MARK: URLConvertible
    public func asURL() throws -> URL {
        return URL(string: "https://api.openweathermap.org" + path)!
    }
}
