//
//  ForecastWeather.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class ForecastWeather: Mappable {
    var identifier: Int?
    var main: String?
    var description: String?
    var icon: WeatherIcon?
    
    required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.identifier <- map["id"]
        self.main <- map["main"]
        self.description <- map["description"]
        self.icon <- map["icon"]
    }
}
