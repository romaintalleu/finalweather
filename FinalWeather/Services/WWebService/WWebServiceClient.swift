//
//  ICWebServiceClient.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RxSwift

class WWebServiceClient {
    private let networkStack: NetworkStack
    
    init() {
        self.networkStack = NetworkStack()
    }
    
    // MARK: - Public methods
    
    // MARK: - Forecast
    
    func fetchForecast() -> Observable<Forecast> {
        return self.sendJSONRequest(url: Route.forecast, method: .get, parameters: Parameter.forecast.value, responseType: Forecast.self)
    }
    
    // MARK: - Private methods
    
    private func sendJSONRequest<T: Mappable>(url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil, responseType: T.Type) -> Observable<T> {
        return self.networkStack.sendJSONRequest(url: url, method: method, parameters: parameters, encoding: encoding, headers: headers, responseType: responseType)
    }
}
