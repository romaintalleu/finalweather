//
//  ForecastHeaderViewModel.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation

struct ForecastHeaderViewModel {
    let cityName: String?
    let countryName: String?
    let highTemp: String?
    let windSpeed: String?
    let humidity: String?
    
    init(forecast: Forecast) {
        self.cityName = forecast.city?.name ?? "Paris"
        self.countryName = forecast.city?.country ?? "France"
        self.highTemp = "15°"
        self.windSpeed = String(format:"%.1fm/s", (forecast.list?.first?.wind?.speed ?? 0))
        self.humidity = "\(forecast.list?.first?.main?.humidity ?? 0)%"
    }
}
