//
//  HomeViewController.swift
//  FinalWeather
//
//  Created by Romain Talleu on 14/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit
import RxSwift

final class HomeViewController: UIViewController, StoryboardBased {
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var forecastHeader: ForecastHeaderView!
    private let disposeBag = DisposeBag()
    
    fileprivate var forecastVM: ForecastViewModel?
    
    var sectionSelected: Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        
        self.requestForecast()
    }
    
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedSectionHeaderHeight = 50.0
        self.tableView.registerHeaderFooterNib(cellClass: ForecastDayTableViewCell.self)
        self.tableView.registerCellNib(cellClass: ForecastHourTableViewCell.self)
        self.tableView.separatorStyle = .none
    }
    
    private func requestForecast() {
        WWebServiceClient().fetchForecast()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { forecast in
                self.forecastVM = ForecastViewModel(forecast: forecast)
                self.forecastHeader.setup(header: self.forecastVM!.header)
                print(self.forecastVM)
                self.tableView.reloadData()
            }, onError: { error in
                print(error)
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(self.disposeBag)
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.forecastVM?.days.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sectionSelected == section ? self.forecastVM?.days[section].hours.count ?? 0 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: ForecastDayTableViewCell.className()) as! ForecastDayTableViewCell
        cell.delegate = self
        cell.setup(forecastDay: self.forecastVM!.days[section], for: section)
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ForecastHourTableViewCell.className(), for: indexPath) as! ForecastHourTableViewCell
        cell.setup(forecastHour: self.forecastVM!.days[indexPath.section].hours[indexPath.row])
        return cell
    }
}

extension HomeViewController: ForecastDayTableViewCellDelegate {
    func headerClicked(section: Int) {
        if self.sectionSelected == section {
            self.sectionSelected = nil
        } else {
            self.sectionSelected = section
        }
        self.tableView.reloadData()
    }
}
