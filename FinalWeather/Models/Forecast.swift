//
//  Forecast.swift
//  FinalWeather
//
//  Created by Romain Talleu on 14/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class Forecast: Mappable {
    var cod: Int?
    var message: Double?
    var list: [ForecastItem]?
    var city: City?
    
    required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.cod <- map["cod"]
        self.message <- map["messages"]
        self.list <- map["list"]
        self.city <- map["city"]
    }
}
