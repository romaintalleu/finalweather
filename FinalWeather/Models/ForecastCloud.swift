//
//  ForecastCloud.swift
//  FinalWeather
//
//  Created by Romain Talleu on 22/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class ForecastCloud: Mappable {
    var all: Int?
    
    required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.all <- map["all"]
    }
}
