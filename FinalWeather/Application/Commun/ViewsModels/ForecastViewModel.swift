//
//  ForecastViewModel.swift
//  FinalWeather
//
//  Created by Romain Talleu on 14/12/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation

struct ForecastViewModel {
    
    let header: ForecastHeaderViewModel
    var days = [ForecastDayViewModel]()
    
    init(forecast: Forecast) {
        self.header = ForecastHeaderViewModel(forecast: forecast)
        
        if let list = forecast.list {
            var list = list
            var date = Date(timeIntervalSince1970: Double(list.first?.dt ?? 0))
            var items = [ForecastItem]()
            items.append(list.first!)
            list.removeFirst()
            for item in list {
                let currentDate = Date(timeIntervalSince1970: Double(item.dt ?? 0))
                if Calendar.current.compare(date, to: currentDate, toGranularity: .day) == .orderedSame {
                    items.append(item)
                } else {
                    self.days.append(ForecastDayViewModel(items: items))
                    items.removeAll()
                    items.append(item)
                    date = currentDate
                }
            }
        }
    }
}
